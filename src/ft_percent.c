/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_percent.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bsemchuk <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/10 14:12:59 by bsemchuk          #+#    #+#             */
/*   Updated: 2017/06/01 15:03:21 by bsemchuk         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/ft_printf.h"

static int	ft_make_width(t_args *elem)
{
	if (elem->width > ft_strlen(elem->aim))
	{
		if (elem->flags.isminu == 1)
			while (ft_strlen(elem->aim) < elem->width)
				elem->aim = ft_straddpostfix(" ", elem->aim);
		if (elem->flags.isminu == 0)
			while (ft_strlen(elem->aim) < elem->width)
			{
				if (elem->flags.iszero == 1)
					elem->aim = ft_straddprefix("0", elem->aim);
				else
					elem->aim = ft_straddprefix(" ", elem->aim);
			}
	}
	return (0);
}

int			ft_percent(va_list *ap, t_args *elem)
{
	va_list	*norme;

	norme = NULL;
	norme = ap;
	elem->aim = ft_strdup("%");
	ft_make_width(elem);
	return (0);
}
