/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_x.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bsemchuk <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/12 15:12:22 by bsemchuk          #+#    #+#             */
/*   Updated: 2017/05/31 21:57:08 by bsemchuk         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/ft_printf.h"

static void			ft_prefix_hex(t_args *elem)
{
	if (elem->spec == 'X')
		elem->aim = ft_straddprefix(UHEXPREF, elem->aim);
	else if (elem->spec == 'x')
		elem->aim = ft_straddprefix(HEXPREF, elem->aim);
}

static int			ft_make_width(t_args *elem, uintmax_t res)
{
	int	x;

	x = (elem->flags.ishash == 1 && elem->flags.iszero == 1) ? 2 : 0;
	if (elem->flags.ishash == 1 && res && elem->flags.iszero == 0)
		ft_prefix_hex(elem);
	if (elem->width > ft_strlen(elem->aim))
	{
		if (elem->flags.isminu == 1)
			while ((ft_strlen(elem->aim) < elem->width - x))
				elem->aim = ft_straddpostfix(" ", elem->aim);
		if (elem->flags.isminu == 0)
			while ((ft_strlen(elem->aim) < elem->width - x))
			{
				if (elem->flags.iszero == 1)
					elem->aim = ft_straddprefix("0", elem->aim);
				else
					elem->aim = ft_straddprefix(" ", elem->aim);
			}
	}
	if (elem->flags.ishash == 1 && res && elem->flags.iszero == 1)
		ft_prefix_hex(elem);
	return (0);
}

static uintmax_t	ft_x_length_ret(t_args *elem, va_list ap)
{
	if (elem->length.l == 1)
		return ((long unsigned)va_arg(ap, intmax_t));
	if (elem->length.hh == 1)
		return ((unsigned char)va_arg(ap, intmax_t));
	if (elem->length.h == 1)
		return ((unsigned short)va_arg(ap, intmax_t));
	if (elem->length.ll == 1)
		return ((unsigned long long)(va_arg(ap, uintmax_t)));
	if (elem->length.j == 1)
		return ((va_arg(ap, uintmax_t)));
	if (elem->length.z == 1)
		return ((size_t)(va_arg(ap, uintmax_t)));
	return (va_arg(ap, unsigned int));
}

static int			ft_make_prec(t_args *elem)
{
	while (elem->precision > ft_strlen(elem->aim))
		elem->aim = ft_straddprefix("0", elem->aim);
	return (0);
}

int					ft_x(va_list *ap, t_args *elem)
{
	uintmax_t	res;
	char		sel;

	while (elem->id == 0 || elem->aim)
		elem = elem->next;
	sel = (elem->spec == 'x') ? 'l' : 'u';
	res = ft_x_length_ret(elem, *ap);
	elem->aim = ft_from_udec(res, 16, sel);
	ft_precision(elem, (intmax_t)res);
	ft_make_prec(elem);
	ft_make_width(elem, res);
	return (0);
}
