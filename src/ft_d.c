/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_d.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bsemchuk <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/10 12:39:14 by bsemchuk          #+#    #+#             */
/*   Updated: 2017/05/31 21:55:24 by bsemchuk         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/ft_printf.h"

int			ft_plus(t_args *elem, intmax_t number)
{
	if (number >= 0 && elem->flags.isspac == 1 && elem->flags.isplus == 0)
		elem->aim = ft_straddprefix(" ", elem->aim);
	else if (number < 0)
		elem->aim = ft_straddprefix("-", elem->aim);
	else if (number >= 0 && elem->flags.isplus == 1)
		elem->aim = ft_straddprefix("+", elem->aim);
	return (0);
}

static int	ft_prefix_till(t_args *e, int n, int *x, int *d)
{
	if ((e->precision != 1 || e->width != 0) && e->flags.iszero == 0)
	{
		ft_plus(e, n);
		*x = 0;
		*d = 1;
	}
	return (0);
}

int			ft_make_prec_d(t_args *elem)
{
	if (elem->precision > ft_strlen(elem->aim))
		while (ft_strlen(elem->aim) < elem->precision)
			elem->aim = ft_straddprefix("0", elem->aim);
	return (0);
}

int			ft_make_width_d(t_args *e, intmax_t n)
{
	int	x;
	int d;

	d = 0;
	x = ((e->flags.isplus == 1 || e->flags.isspac == 1) || (n < 0)) ? 1 : 0;
	if (e->width == 0)
		return (0);
	ft_prefix_till(e, n, &x, &d);
	if (e->width > (ft_strlen(e->aim) + x))
	{
		if (e->flags.isminu == 1)
			while ((ft_strlen(e->aim) + x) < e->width)
				e->aim = ft_straddpostfix(" ", e->aim);
		if (e->flags.isminu == 0)
			while ((ft_strlen(e->aim) + x) < e->width)
			{
				if (e->flags.iszero == 1 && e->precision == 1)
					e->aim = ft_straddprefix("0", e->aim);
				else
					e->aim = ft_straddprefix(" ", e->aim);
			}
	}
	return (d);
}

int			ft_d(va_list *ap, t_args *e)
{
	intmax_t	number;

	number = ft_d_length_ret(e, *ap);
	while (e->id == 0 || e->aim)
		e = e->next;
	e->aim = ft_usitoa(number);
	ft_precision(e, number);
	ft_make_prec_d(e);
	if (ft_make_width_d(e, number) != 1)
		ft_plus(e, number);
	return (0);
}
