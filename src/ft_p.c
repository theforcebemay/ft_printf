/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_p.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bsemchuk <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/10 10:37:36 by bsemchuk          #+#    #+#             */
/*   Updated: 2017/05/31 21:58:08 by bsemchuk         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/ft_printf.h"

static int	ft_make_prec(t_args *elem)
{
	if (elem->precision > ft_strlen(elem->aim))
		while (ft_strlen(elem->aim) < elem->precision)
			elem->aim = ft_straddprefix("0", elem->aim);
	return (0);
}

static int	ft_make_width(t_args *elem)
{
	while (elem->width > ft_strlen(elem->aim))
	{
		if (elem->flags.isminu == 0)
			elem->aim = ft_straddprefix(" ", elem->aim);
		else
			elem->aim = ft_straddpostfix(" ", elem->aim);
	}
	return (0);
}

int			ft_p(va_list *ap, t_args *elem)
{
	void				*p;
	long unsigned int	ui;

	p = va_arg(*ap, void *);
	ui = (long unsigned int)p;
	if (ui == 0)
		elem->aim = ft_strdup("");
	else
		elem->aim = (char *)ft_from_udec(ui, 16, 'l');
	ft_make_prec(elem);
	elem->aim = ft_straddprefix(HEXPREF, (char *)elem->aim);
	ft_make_width(elem);
	return (0);
}
