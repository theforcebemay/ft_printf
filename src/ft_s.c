/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_s.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bsemchuk <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/10 11:50:02 by bsemchuk          #+#    #+#             */
/*   Updated: 2017/06/01 14:38:48 by bsemchuk         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/ft_printf.h"

int			ft_s(va_list *ap, t_args *e)
{
	char	*debt;
	int		end;
	int		start;
	int		fill;

	if (e->length.l == 1)
	{
		ft_su(ap, e);
		return (0);
	}
	while (e->id == 0 || e->aim)
		e = e->next;
	debt = va_arg(*ap, char *);
	debt = (!debt) ? ft_strdup("(null)") : debt;
	e->precision = (e->precision == 1 ||\
			ft_strlen(debt) < e->precision) ? ft_strlen(debt) : e->precision;
	end = (e->precision);
	end = (!*debt && e->width > e->precision) ? 0 : end;
	e->width = (e->width > e->precision) ? e->width : e->precision;
	e->aim = ft_strnew(e->width);
	fill = (e->flags.iszero == 1) ? '0' : ' ';
	start = (e->flags.isminu == 1) ? 0 : (e->width - end);
	ft_memset(e->aim, fill, e->width);
	ft_strncpy((e->aim + start), debt, end);
	return (0);
}
