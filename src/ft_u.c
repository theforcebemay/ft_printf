/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_u.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bsemchuk <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/12 21:56:54 by bsemchuk          #+#    #+#             */
/*   Updated: 2017/05/31 21:56:52 by bsemchuk         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/ft_printf.h"

static int			ft_make_width(t_args *elem)
{
	if (elem->width > ft_strlen(elem->aim))
	{
		if (elem->flags.isminu == 1)
			while (ft_strlen(elem->aim) < elem->width)
				elem->aim = ft_straddpostfix(" ", elem->aim);
		if (elem->flags.isminu == 0)
			while (ft_strlen(elem->aim) < elem->width)
			{
				if (elem->flags.iszero == 1)
					elem->aim = ft_straddprefix("0", elem->aim);
				else
					elem->aim = ft_straddprefix(" ", elem->aim);
			}
	}
	return (0);
}

static int			ft_make_prec(t_args *elem)
{
	if (elem->precision > ft_strlen(elem->aim))
		while (ft_strlen(elem->aim) < elem->precision)
			elem->aim = ft_straddprefix("0", elem->aim);
	return (0);
}

static uintmax_t	ft_u_length_ret(t_args *elem, va_list ap)
{
	if (elem->length.l == 1)
		return ((long unsigned)va_arg(ap, intmax_t));
	if (elem->length.hh == 1)
		return ((unsigned char)va_arg(ap, intmax_t));
	if (elem->length.h == 1)
		return ((unsigned short)va_arg(ap, intmax_t));
	if (elem->length.ll == 1)
		return ((unsigned long long)(va_arg(ap, uintmax_t)));
	if (elem->length.j == 1)
		return ((va_arg(ap, uintmax_t)));
	if (elem->length.z == 1)
		return ((va_arg(ap, long long unsigned)));
	return (va_arg(ap, unsigned int));
}

int					ft_u(va_list *ap, t_args *elem)
{
	uintmax_t	ui;

	ui = ft_u_length_ret(elem, *ap);
	elem->aim = ft_uitoa(ui);
	ft_make_prec(elem);
	ft_precision(elem, (intmax_t)ui);
	ft_make_width(elem);
	return (0);
}
