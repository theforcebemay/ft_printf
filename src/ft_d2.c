/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_d2.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bsemchuk <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/10 12:39:14 by bsemchuk          #+#    #+#             */
/*   Updated: 2017/05/31 21:31:46 by bsemchuk         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/ft_printf.h"

int			ft_precision(t_args *elem, intmax_t number)
{
	if (elem->precision == 0 && !number)
	{
		free(elem->aim);
		elem->aim = ft_strdup("");
		return (1);
	}
	return (0);
}

intmax_t	ft_d_length_ret(t_args *elem, va_list ap)
{
	if (elem->length.l == 1)
		return ((long)va_arg(ap, intmax_t));
	if (elem->length.hh == 1)
		return ((signed char)va_arg(ap, intmax_t));
	if (elem->length.h == 1)
		return ((short)va_arg(ap, intmax_t));
	if (elem->length.ll == 1)
		return ((long long int)(va_arg(ap, long long int)));
	if (elem->length.j == 1)
		return ((va_arg(ap, intmax_t)));
	if (elem->length.z == 1)
		return ((size_t)(va_arg(ap, intmax_t)));
	return (va_arg(ap, int));
}
