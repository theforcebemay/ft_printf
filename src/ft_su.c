/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_S.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bsemchuk <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/10 10:34:02 by bsemchuk          #+#    #+#             */
/*   Updated: 2017/06/01 14:39:37 by bsemchuk         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/ft_printf.h"

static int	ft_size(wint_t character)
{
	if (character <= 127)
		return (1);
	if (character <= 2047)
		return (2);
	if (character <= 65535)
		return (3);
	return (4);
}

static int	ft_finish(int precision, wchar_t *wchar)
{
	int	size;

	size = 0;
	while (precision >= size + ft_size(wchar[0]))
		size += ft_size(*wchar++);
	return (size);
}

static int	ft_wcsbcpy(int i, char *to, wchar_t *fr)
{
	int	k;

	k = 0;
	while (k < i)
		k += (ft_wctomb(&to[k], *fr++));
	return (i);
}

static int	ft_nullcheck(t_args *elem, wchar_t *pointer)
{
	if (!pointer)
	{
		elem->aim = ft_strdup("(null)");
		elem->width = 6;
		return (0);
	}
	return (1);
}

int			ft_su(va_list *ap, t_args *elem)
{
	wchar_t		*wstr;
	unsigned	end;
	int			start;
	int			fill;

	fill = (elem->flags.iszero == 1) ? '0' : ' ';
	wstr = va_arg(*ap, wchar_t *);
	if (!ft_nullcheck(elem, wstr))
		return (0);
	if (elem->precision == 1)
		elem->precision = ft_wcblen(wstr);
	end = ft_finish(elem->precision, wstr);
	elem->width = (elem->width > end) ? elem->width : end;
	elem->aim = ft_strnew(elem->width);
	ft_memset(elem->aim, fill, elem->width);
	start = (elem->flags.isminu == 1) ? 0 : (elem->width - end);
	ft_wcsbcpy(end, (elem->aim + start), wstr);
	return (0);
}
