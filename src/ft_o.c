/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_o.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bsemchuk <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/12 14:51:02 by bsemchuk          #+#    #+#             */
/*   Updated: 2017/05/31 21:55:59 by bsemchuk         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/ft_printf.h"

uintmax_t	ft_o_length_ret(t_args *elem, va_list ap)
{
	if (elem->length.l == 1)
		return ((long unsigned)va_arg(ap, intmax_t));
	if (elem->length.hh == 1)
		return ((unsigned char)va_arg(ap, int));
	if (elem->length.h == 1)
		return ((unsigned short)va_arg(ap, intmax_t));
	if (elem->length.ll == 1)
		return ((va_arg(ap, unsigned long long)));
	if (elem->length.j == 1)
		return ((va_arg(ap, uintmax_t)));
	if (elem->length.z == 1)
		return ((va_arg(ap, size_t)));
	return (va_arg(ap, unsigned int));
}

static int	ft_make_width(t_args *elem)
{
	if (elem->width > ft_strlen(elem->aim))
	{
		if (elem->flags.isminu == 1)
			while (ft_strlen(elem->aim) < elem->width)
				elem->aim = ft_straddpostfix(" ", elem->aim);
		if (elem->flags.isminu == 0)
			while (ft_strlen(elem->aim) < elem->width)
			{
				if (elem->flags.iszero == 1)
					elem->aim = ft_straddprefix("0", elem->aim);
				else
					elem->aim = ft_straddprefix(" ", elem->aim);
			}
	}
	return (0);
}

static int	ft_make_prec(t_args *elem)
{
	if (elem->precision > ft_strlen(elem->aim))
		while (ft_strlen(elem->aim) < elem->precision)
			elem->aim = ft_straddprefix("0", elem->aim);
	return (0);
}

int			ft_o(va_list *ap, t_args *elem)
{
	uintmax_t	res;

	while (elem->id == 0 || elem->aim)
		elem = elem->next;
	res = ft_o_length_ret(elem, *ap);
	elem->aim = ft_from_udec((uintmax_t)res, 8, 'l');
	ft_make_prec(elem);
	ft_precision(elem, res);
	if ((elem->flags.ishash == 1 && (elem->aim[0] != '0' || elem->aim[0] == 0)))
		elem->aim = ft_straddprefix(OCTPREF, elem->aim);
	ft_make_width(elem);
	return (0);
}
