/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_c.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bsemchuk <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/12 14:40:07 by bsemchuk          #+#    #+#             */
/*   Updated: 2017/05/31 21:57:36 by bsemchuk         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/ft_printf.h"

int			ft_c(va_list *ap, t_args *elem)
{
	char		received;
	char		fill;
	unsigned	pos;

	if (elem->length.l == 1)
		return (ft_cu(ap, elem));
	fill = (elem->flags.iszero == 1) ? '0' : ' ';
	received = (char)va_arg(*ap, int);
	elem->nothing = (received == 0) ? 1 : 0;
	if (elem->width == 0)
		elem->width = 1;
	if (elem->width == 1)
	{
		elem->aim = ft_strnew(1);
		elem->aim[0] = received;
	}
	else
	{
		elem->aim = ft_strnew((size_t)elem->width);
		elem->aim = ft_memset((void *)elem->aim, fill, elem->width);
		pos = (elem->flags.isminu == 1) ? 0 : elem->width - 1;
		elem->aim[pos] = received;
	}
	return (0);
}
