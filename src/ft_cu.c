/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_cu.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bsemchuk <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/12 21:03:25 by bsemchuk          #+#    #+#             */
/*   Updated: 2017/05/31 21:53:22 by bsemchuk         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/ft_printf.h"

static int	ft_size(wint_t character)
{
	if (character <= 127)
		return (1);
	if (character <= 2047)
		return (2);
	if (character <= 65535)
		return (3);
	return (4);
}

int			ft_cu(va_list *ap, t_args *elem)
{
	wchar_t			character;
	unsigned int	size;

	character = (wchar_t)va_arg(*ap, wint_t);
	elem->nothing = (!character) ? 1 : 0;
	size = ft_size(character);
	elem->width = (elem->width > size) ? elem->width : size;
	elem->aim = ft_strnew(elem->width);
	ft_memset(elem->aim, ' ', elem->width);
	size = (elem->flags.isminu == 1) ? 0 : (elem->width - size);
	elem->width = ft_wctomb(&elem->aim[size], character);
	return (0);
}
