# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: bsemchuk <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2017/03/31 20:12:19 by bsemchuk          #+#    #+#              #
#    Updated: 2017/06/01 15:00:11 by bsemchuk         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = libftprintf.a

SRC_PATH = ./src

INC_PATH = ./includes

LIB_PATH = ./libft

FT_PRINTF_PATH = ./ft_printf


OBJ_PATH = ./obj

OBJLIB_PATH = ./obj

OBJ_FT_PRINTF_PATH = ./obj


LIB_HEADER = libft.h


SRC_NAME = ft_c.c ft_cu.c ft_d.c ft_d2.c ft_du.c ft_i.c ft_o.c ft_ou.c ft_p.c ft_percent.c ft_s.c ft_su.c ft_u.c ft_uu.c ft_x.c ft_xu.c 

FT_PRINTF_NAME = from_decimal.c from_udecimal.c ft_lists.c ft_formatting.c ft_print_items.c ft_printf.c ft_convert_value.c ft_unknown.c ft_formatting2.c


INC_NAME = ft_printf.h

LIB_NAME = ft_atoi.c ft_itoa.c ft_putstr.c ft_strachr.c ft_straddprefix.c ft_strchr.c ft_strcpy.c ft_strdup.c ft_strlchr.c \
		   ft_strlen.c ft_strnmcpy.c ft_strspn.c ft_uitoa.c ft_straddpostfix.c ft_strncpy.c ft_strclr.c ft_usitoa.c ft_isdigit.c ft_strnew.c ft_bzero.c \
		   ft_memset.c ft_wctomb.c ft_wstrtombstr.c ft_wcblen.c ft_wcslen.c ft_memcpy.c ft_wcsncpy.c


OBJ_NAME = $(SRC_NAME:.c=.o)

OBJLIB_NAME = $(LIB_NAME:.c=.o)

OBJ_FT_PRINTF_NAME = $(FT_PRINTF_NAME:.c=.o)


SRC = $(addprefix $(SRC_PATH)/, $(SRC_NAME))
LIB = $(addprefix $(LIB_PATH)/, $(LIB_NAME))
INC = $(addprefix $(INC_PATH)/, $(INC_NAME))
OBJ = $(addprefix $(OBJ_PATH)/, $(OBJ_NAME))
OBJLIB = $(addprefix $(OBJLIB_PATH)/, $(OBJLIB_NAME))
OBJFTPRINTF = $(addprefix $(OBJ_FT_PRINTF_PATH)/, $(OBJ_FT_PRINTF_NAME))


CC = gcc

FLAGS = -g #-Wall -Wextra -Werror

all: $(NAME)

$(NAME): $(OBJ) $(OBJLIB) $(OBJFTPRINTF) 
	@ar rc $(NAME) $(OBJ) $(OBJLIB) $(OBJFTPRINTF) 
	@ranlib $(NAME)
	@echo "                    \033[1;34mD O N E !\033[0m"
	@echo "\033[1;34mft_printf\t\033[1;33mCompilation\t\033[0;32m[OK]\033[0m"

$(OBJ_PATH)/%.o: $(SRC_PATH)/%.c
	@mkdir $(OBJ_PATH) 2> /dev/null || true
	@$(CC) $(FLAGS) -o $@ -c $<

$(OBJLIB_PATH)/%.o: $(LIB_PATH)/%.c
	@mkdir $(OBJLIB_PATH) 2> /dev/null || true
	@$(CC) $(FLAGS) -o $@ -c $<

$(OBJ_FT_PRINTF_PATH)/%.o: $(FT_PRINTF_PATH)/%.c
	@mkdir $(OBJ_FT_PRINTF_PATH) 2> /dev/null || true
	@$(CC) $(FLAGS) -o $@ -c $<

clean:
	@rm -rf $(OBJ) $(OBJLIB) $(OBJFORMAT) $(OBJFTPRINTF)
	@echo "\033[1;34mft_printf\t\033[1;33mCleaning obj\t\033[0;32m[OK]\033[0m"
	@make clean -C libft/

fclean: clean
	@rm -rf ./obj $(NAME)
	@echo "\033[1;34mft_printf\t\033[1;33mCleaning lib\t\033[0;32m[OK]\033[0m"
	@make fclean -C libft/

re: $(MAKE) all

.PHONY: all clean fclean re norme
