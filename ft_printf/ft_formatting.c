/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_formatting.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bsemchuk <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/17 19:00:47 by bsemchuk          #+#    #+#             */
/*   Updated: 2017/06/01 14:41:58 by bsemchuk         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/ft_printf.h"

t_args	*ft_fill_structure(const char *formatted)
{
	const char	*temp;
	t_args		*elem;
	t_args		*begin;
	int			n;

	begin = NULL;
	while ((temp = ft_strchr(formatted, '%')))
	{
		elem = ft_create_elem();
		if (!begin)
			begin = ft_create_elem();
		n = ft_strspn(ALL, ++temp);
		elem->what = ft_strnmcpy(temp, ++n);
		ft_pushback(begin, elem);
		formatted = temp + n;
		parse_elem(elem);
	}
	return (begin);
}

void	parse_elem(t_args *elem)
{
	add_flags_to_structure(elem);
	add_width_to_structure(elem);
	add_precision_to_structure(elem);
	add_length_to_structure(elem);
	elem->spec = ft_strlchr(elem->what);
}
