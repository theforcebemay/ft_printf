/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   from_decimal.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bsemchuk <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/31 16:34:26 by bsemchuk          #+#    #+#             */
/*   Updated: 2017/06/01 14:50:23 by bsemchuk         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/ft_printf.h"

/*
**	Counts number length including minus for malloc;
*/

int			ft_numlen(intmax_t number)
{
	int len;

	len = 0;
	if (number < 0)
	{
		number = -number;
		len++;
	}
	while (number != 0)
	{
		len++;
		number /= 10;
	}
	return (len);
}

/*
**	What kind of man will do work like that without hands?
**	Handless...
**	Heartless...
**	Beatless...
**	Less...
*/

char		*ft_from_dec(intmax_t number, int base, char c)
{
	char	*use;
	char	*to;
	int		position;

	if (number == 0)
		return (ft_strdup("0"));
	to = (char *)malloc(sizeof(char) * ft_numlen(number) + 1);
	if (c == 'l')
		use = "0123456789abcdef";
	else
		use = "0123456789ABCDEF";
	position = ft_numlen(number);
	to[position--] = '\0';
	while (number)
	{
		to[position--] = use[number % base];
		number = number / base;
	}
	use = ft_strdup(&to[++position]);
	free(to);
	return (use);
}
