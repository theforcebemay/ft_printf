/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lists.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bsemchuk <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/01 14:44:00 by bsemchuk          #+#    #+#             */
/*   Updated: 2017/05/31 20:16:35 by bsemchuk         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/ft_printf.h"

/*
**	void	printlist(t_args *list)
**	{
**		while (list)
**		{
**			printf("id	=		%d\n", list->id);
**			printf("what	=		%s\n", list->what);
**			printf("spec	=		%c\n", list->spec);
**			printf("width	=		%d\n", list->width);
**			printf("aim	=		%s\n", list->aim);
**			printf("precis	=		%d\n", list->precision);
**			printf("length:\n");
**			printf("hh	=		%d\n", list->length.hh);
**			printf("h	=		%d\n", list->length.h);
**			printf("l	=		%d\n", list->length.l);
**			printf("ll	=		%d\n", list->length.ll);
**			printf("j	=		%d\n", list->length.j);
**			printf("t	=		%d\n", list->length.t);
**			printf("z	=		%d\n", list->length.z);
**			printf("flags:\n");
**			printf("ishash	=		%d\n", list->flags.ishash);
**			printf("iszero	=		%d\n", list->flags.iszero);
**			printf("isminu	=		%d\n", list->flags.isminu);
**			printf("isspac	=		%d\n", list->flags.isspac);
**			printf("isplus	=		%d\n", list->flags.isplus);
**			printf("next	=		%p\n", list->next);
**			printf("\n");
**			list = list->next;
**		}
**	}
*/

t_args	*ft_create_elem(void)
{
	t_args	*debt;

	if (!(debt = (t_args *)malloc(sizeof(t_args))))
		return (NULL);
	debt->id = 0;
	debt->what = NULL;
	debt->aim = NULL;
	debt->width = 0;
	debt->spec = 0;
	debt->precision = 1;
	debt->next = NULL;
	debt->nothing = 0;
	debt->length.hh = 0;
	debt->length.h = 0;
	debt->length.l = 0;
	debt->length.ll = 0;
	debt->length.j = 0;
	debt->length.t = 0;
	debt->length.z = 0;
	debt->flags.ishash = 0;
	debt->flags.iszero = 0;
	debt->flags.isminu = 0;
	debt->flags.isspac = 0;
	debt->flags.isplus = 0;
	return (debt);
}

int		ft_free_django(t_args *begin)
{
	t_args	*temp;

	while (begin->next)
	{
		temp = begin;
		if (begin->what)
			free(begin->what);
		if (begin->aim)
			free(begin->aim);
		if (begin->next)
			begin = begin->next;
		free(temp);
	}
	return (0);
}

t_args	*ft_pushback(t_args *start, t_args *added)
{
	int		id;

	if (!added)
		return (NULL);
	if (!start)
	{
		start = ft_create_elem();
		start->next = added;
		return (start);
	}
	while (start->next)
		start = start->next;
	id = start->id;
	start->next = added;
	start = start->next;
	start->id = ++id;
	return (NULL);
}
