/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_formatting2.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bsemchuk <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/17 19:00:47 by bsemchuk          #+#    #+#             */
/*   Updated: 2017/06/01 14:57:22 by bsemchuk         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/ft_printf.h"

int		ft_abs(int n)
{
	if (n < 0)
		return (-n);
	return (n);
}

void	add_precision_to_structure(t_args *elem)
{
	char	*offset;

	offset = NULL;
	if (elem->id != 0)
	{
		if ((offset = ft_strchr(elem->what, '.')))
			elem->precision = ft_abs(ft_atoi(offset));
		if (offset)
			elem->precision = ft_abs(ft_atoi(++offset));
	}
}

void	add_width_to_structure(t_args *elem)
{
	int	res;

	if (elem->id != 0)
	{
		res = ft_strspn(FLAGS, elem->what);
		if (elem->what[res - 1] != '.' && ft_isdigit(elem->what[res]))
		{
			res = ft_atoi(&elem->what[res]);
			if (res < 0)
				res = -res;
			elem->width = res;
		}
	}
}

void	add_length_to_structure(t_args *elem)
{
	char			*res;
	unsigned int	i;

	i = 0;
	while (i < ft_strlen(LENGTHF))
	{
		res = ft_strchr(elem->what, LENGTHF[i]);
		if (res)
		{
			if (i == 0 && (*(res + 1) == *res))
				elem->length.hh = 1;
			if (i == 0 && (*(res + 1) != *res))
				elem->length.h = 1;
			if (i == 1 && (*(res + 1) == *res))
				elem->length.ll = 1;
			if (i == 1 && (*(res + 1) != *res))
				elem->length.l = 1;
			if (i == 2)
				elem->length.j = 1;
			elem->length.t = (i == 3) ? 1 : 0;
			elem->length.z = (i == 4) ? 1 : 0;
		}
		i++;
	}
}

void	add_flags_to_structure(t_args *elem)
{
	int		i;
	char	*temp;
	int		end;

	i = 0;
	end = ft_strspn(FLAGS, elem->what);
	while (FLAGS[i])
	{
		if ((temp = ft_strchr(elem->what, FLAGS[i])) != NULL)
		{
			if (i == 0)
				elem->flags.ishash = 1;
			if (i == 1)
				elem->flags.isspac = 1;
			if (i == 2)
				elem->flags.isminu = 1;
			if (i == 3)
				elem->flags.isplus = 1;
			if (i == 4 && temp < (elem->what + end))
				elem->flags.iszero = 1;
		}
		i++;
	}
}
