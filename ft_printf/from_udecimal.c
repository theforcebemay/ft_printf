/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   from_udecimal.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bsemchuk <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/31 16:34:26 by bsemchuk          #+#    #+#             */
/*   Updated: 2017/06/01 14:50:33 by bsemchuk         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/ft_printf.h"

/*
**	What kind of man will do work like that without hands?
**	Handless...
**	Heartless...
**	Beatless...
**	Less...
*/

static int	ft_numlen(uintmax_t number)
{
	int len;

	len = 0;
	while (number != 0)
	{
		len++;
		number /= 10;
	}
	return (len);
}

char		*ft_from_udec(uintmax_t number, int base, char c)
{
	char	*use;
	char	*to;
	int		position;

	if (number == 0)
		return (ft_strdup("0"));
	to = (char *)malloc(sizeof(char) * ft_numlen(number) + 1);
	if (c == 'l')
		use = "0123456789abcdef";
	else if (c == 'u')
		use = "0123456789ABCDEF";
	position = ft_numlen(number);
	to[position--] = '\0';
	while (number)
	{
		to[position--] = use[number % base];
		number = number / base;
	}
	use = ft_strdup(&to[++position]);
	return (use);
}
