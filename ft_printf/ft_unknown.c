/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_unknown.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bsemchuk <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/15 19:51:24 by bsemchuk          #+#    #+#             */
/*   Updated: 2017/05/21 19:10:08 by bsemchuk         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/ft_printf.h"

int			ft_unknown(t_args *elem, char ch)
{
	char		fill;
	unsigned	pos;

	fill = (elem->flags.iszero == 1) ? '0' : ' ';
	elem->nothing = (ch == 0) ? 1 : 0;
	if (elem->width == 0)
		elem->width = 1;
	if (elem->width == 1)
	{
		elem->aim = ft_strnew(1);
		elem->aim[0] = ch;
	}
	else
	{
		elem->aim = ft_strnew((size_t)elem->width);
		elem->aim = ft_memset((void *)elem->aim, fill, elem->width);
		pos = (elem->flags.isminu == 1) ? 0 : elem->width - 1;
		elem->aim[pos] = ch;
	}
	return (0);
}
