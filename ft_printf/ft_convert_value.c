/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_stdout.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bsemchuk <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/08 16:46:04 by bsemchuk          #+#    #+#             */
/*   Updated: 2017/06/01 14:45:04 by bsemchuk         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/ft_printf.h"

int		(*g_p[15])(va_list *ap, t_args *elem) =
{ft_s, ft_su, ft_p, ft_d, ft_du, ft_i, ft_o,
	ft_ou, ft_u, ft_uu, ft_x, ft_xu, ft_c, ft_c, ft_percent};

int		ft_parse_elems_for_aim(va_list *ap, t_args *elem)
{
	int			i;

	i = 0;
	while (elem)
	{
		elem = (elem->id == 0) ? elem->next : elem;
		if (SPECIFICATORS[i] == '\0')
		{
			ft_unknown(elem, elem->spec);
			elem = elem->next;
			i = 0;
		}
		else if (SPECIFICATORS[i] == elem->spec)
		{
			(*g_p[i])(ap, elem);
			i = 0;
			if (!elem->next)
				return (0);
			elem = elem->next;
		}
		else
			i++;
	}
	return (0);
}
