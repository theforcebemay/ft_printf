/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bsemchuk <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/17 18:50:24 by bsemchuk          #+#    #+#             */
/*   Updated: 2017/06/01 14:46:55 by bsemchuk         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/ft_printf.h"

int		ft_printf(const char *formatted, ...)
{
	va_list		ap;
	t_args		*args_info;
	int			return_value;

	if (!(ft_strchr(formatted, '%')))
		return (return_value = write(1, formatted, ft_strlen(formatted)));
	va_start(ap, formatted);
	args_info = ft_fill_structure(formatted);
	ft_parse_elems_for_aim(&ap, args_info);
	f(args_info);
	return_value = ft_print_items(formatted, args_info);
	va_end(ap);
	return (return_value);
}
