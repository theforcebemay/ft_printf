/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_items.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bsemchuk <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/14 16:50:56 by bsemchuk          #+#    #+#             */
/*   Updated: 2017/05/31 15:19:41 by bsemchuk         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/ft_printf.h"

/*
**	Print items from elem->aim (as char *)
**	and return length of printed elements
**	(ft_strlen used)
*/

int		ft_print_items(const char *format, t_args *elem)
{
	ssize_t	l;

	l = 0;
	elem = (elem->id == 0) ? elem->next : elem;
	while (*format)
	{
		if (*format != '%' && *format)
			l += write(1, &(*format++), 1);
		else
		{
			l += write(1, (elem->aim), (elem->width));
			elem = (elem->next) ? elem->next : elem;
			format++;
			if (!*format)
				break ;
			format += ft_strspn(ALL, format);
			format++;
		}
	}
	return (l);
}

/*
**	As far as write function takes third argument (bytes to write) from
**	ft_strlen() function, and sometimes happens so we need to write \0
**	what happens to be the dead end for strlen, we will take width from t_args
**	to write length we need to be printed on stdout, and function f() adjusts
**	elem->width to length we need
*/

void	f(t_args *elem)
{
	while (elem)
	{
		if (((elem->spec == 'c' || elem->spec == 'C') && elem->nothing == 1)
				|| elem->spec == 'S')
			elem->width = elem->width;
		else if (elem->spec == 0)
			elem->width = 0;
		else
			elem->width = ft_strlen(elem->aim);
		elem = elem->next;
	}
}
