/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bsemchuk <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/16 17:18:53 by bsemchuk          #+#    #+#             */
/*   Updated: 2017/06/01 14:44:29 by bsemchuk         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_PRINTF_H
# define FT_PRINTF_H
# include "../libft/libft.h"
# include <unistd.h>
# include <stdarg.h>
# include <stdint.h>
# include <stdlib.h>
# include <wchar.h>
# include <locale.h>
# define SUCCESS 0
# define FAIL -1
# define FLAGS "# -+0"
# define LENGTHF "hljtz"
# define SPECIFICATORS "sSpdDioOuUxXcC%"
# define ALL "hljtz# .-+0123456789"
# define HEX 16
# define OCTAL 8
# define HEXPREF "0x"
# define UHEXPREF "0X"
# define OCTPREF "0"

/*
**	FLAGS FORMATTED HERE
*/

typedef struct		s_flags
{
	unsigned int	ishash : 1;
	unsigned int	iszero : 1;
	unsigned int	isminu : 1;
	unsigned int	isspac : 1;
	unsigned int	isplus : 1;
}					t_flags;

typedef struct		s_length
{
	unsigned int	hh : 1;
	unsigned int	h : 1;
	unsigned int	l : 1;
	unsigned int	ll : 1;
	unsigned int	j : 1;
	unsigned int	t : 1;
	unsigned int	z : 1;
}					t_length;

typedef	struct		s_args
{
	unsigned int	id;
	char			*what;
	char			*aim;
	char			spec;
	unsigned int	width;
	unsigned int	precision;
	unsigned int	nothing;
	t_length		length;
	t_flags			flags;
	struct s_args	*next;
}					t_args;

/*
**	There journey begins
*/

int					ft_printf(const char *formatted, ...);

/*
**	Just print all!
*/

int					ft_print_items(const char *format, t_args *elem);

/*
**	Converters
*/

intmax_t			to_hexadecimal(intmax_t number);
intmax_t			to_base(intmax_t number);
intmax_t			ft_atoi_base(intmax_t number, int base);
char				*ft_from_dec(intmax_t number, int base, char c);
char				*ft_from_udec(uintmax_t number, int base, char c);
intmax_t			ft_d_length_ret(t_args *elem, va_list ap);

/*
**	Parsing string and returning array consisted of %...[d,s,o...]
*/

int					ft_parse(const char *format, va_list *ap);
int					ft_parse_elems_for_aim(va_list *ap, t_args *elem);
t_args				*ft_fill_structure(const char *formatted);
void				parse_elem(t_args *elem);
void				add_flags_to_structure(t_args *elem);
void				add_width_to_structure(t_args *elem);
void				add_precision_to_structure(t_args *elem);
void				add_length_to_structure(t_args *elem);
void				add_specificator_to_structure(t_args *elem);
int					ft_width(va_list ap, t_args *elem);
int					ft_precision(t_args *elem, intmax_t number);
int					ft_make_value(t_args *args_info, va_list *ap);

/*
**	Lists manipulations
*/

t_args				*ft_create_elem(void);
int					ft_free_django(t_args *begin);
t_args				*ft_pushback(t_args *start, t_args *added);
void				printlist(t_args *list);
void				f(t_args *elem);

/*
**	Record va_list value as char in elem->aim, to make precision after
*/

int					gotofunction(const char *format, va_list *ap, t_args *elem);
int					ft_return_pointer_position(t_args *elem);

/*
**	Pointer dispatcher
*/

int					ft_pointer_dispatcher(const char *format,
		va_list *ap, t_args *elem);

/*
**	List of functions pointed to
*/

int					ft_s(va_list *ap, t_args *elem);
int					ft_su(va_list *ap, t_args *elem);
int					ft_p(va_list *ap, t_args *elem);
int					ft_d(va_list *ap, t_args *elem);
int					ft_du(va_list *ap, t_args *elem);
int					ft_i(va_list *ap, t_args *elem);
int					ft_o(va_list *ap, t_args *elem);
int					ft_ou(va_list *ap, t_args *elem);
int					ft_u(va_list *ap, t_args *elem);
int					ft_uu(va_list *ap, t_args *elem);
int					ft_x(va_list *ap, t_args *elem);
int					ft_xu(va_list *ap, t_args *elem);
int					ft_c(va_list *ap, t_args *elem);
int					ft_cu(va_list *ap, t_args *elem);
int					ft_percent(va_list *ap, t_args *elem);

/*
**	Precision check
**	for zero and no precision
*/

int					ft_check_prec_one(t_args *elem);
int					ft_check_prec_zero(t_args *elem, intmax_t number);
int					ft_unknown(t_args *elem, char ch);

/*
**	other!
*/

int					ft_make_prec_d(t_args *elem);
int					ft_make_width_d(t_args *e, intmax_t n);

#endif
